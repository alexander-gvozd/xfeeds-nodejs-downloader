const syncRequest = require( 'sync-request' );
const config = require( '../config/config' );

const Proxy = {
  proxyList: null,
  _proxyListsUrls: null,

  _init: function () {
    try {
      this._proxyListsUrls = this._getProxyListsUrls();
      this._laodProxiesList();
    } catch {
      console.error( 'Error: Proxy not loaded' );
      this.proxyList = null;
    }
  },

  _getProxyListsUrls: function ( url ) {
    function formatDate( date ) {
      const year = date.getFullYear();
      const month = date.getMonth() + 1;
      const day = date.getDate() > 10 ? date.getDate() : '0' + date.getDate();
      return `${ year }-${ month }-${ day }`;
    }

    const date = new Date();
    const yesterdayDate = new Date( Date.now() - 1000 * 60 * 60 * 24 );
    const today = config.proxyListUrl + '/' + formatDate( date );
    const yesterday = config.proxyListUrl + '/' + formatDate( yesterdayDate );

    return { new: today, old: yesterday };

  },

  _laodProxiesList: function () {
    try {
      const proxyList = JSON.parse( syncRequest( 'GET', this._proxyListsUrls.new ).getBody( 'utf8' ) );

      if ( typeof proxyList === "object" && proxyList.length ) {
        this.proxyList = proxyList;
      } else {
        console.log( 'ERROR' );
        throw console.log( 'no new proxy list' );
      }
    } catch {
      const proxyList = JSON.parse( syncRequest( 'GET', this._proxyListsUrls.old ).getBody( 'utf8' ) );

      if ( typeof proxyList === "object" && proxyList.length ) {
        this.proxyList = proxyList;
      } else {
        throw console.log( 'no old proxy list' );
      }
    }
  },

  getProxyList: function ( url ) {
    if ( this.proxyList === null ) {
      return null;
    }

    const reHttp = RegExp( /^http:/ );
    const reHttps = RegExp( /^https:/ );

    let proxyList = null;

    // filter by protocol
    if ( url.search( reHttp ) === 0 ) {
      proxyList = this.proxyList.filter( function ( proxy ) {
        return proxy.type === 1;
      } );
    } else if ( url.search( reHttps ) === 0 ) {
      proxyList = this.proxyList.filter( function ( proxy ) {
        return proxy.type === 2;
      } );
    }

    return proxyList.filter( function ( proxy ) {
      return proxy.kind === 2 && proxy.timeout < 1000 * config.maxProxyTimeoutS;
    } );
  },

  getRandomProxy: function ( url ) {
    const proxyList = this.getProxyList( url );

    if ( !proxyList || proxyList.length === 0 ) {
      return null;
    }

    return proxyList[ Math.floor( Math.random() * proxyList.length ) ].addr;
  },
};

Proxy._init();

module.exports = Proxy;
