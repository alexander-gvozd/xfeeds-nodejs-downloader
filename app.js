const config = require( './config/config' );
const Proxy = require( './src/proxy' );
const express = require( 'express' );
const app = express();
const request = require( 'request' );
const bodyParser = require( 'body-parser' );
const jsonParser = bodyParser.json( { limit: config.serverMaxPostSize } );

app.use( express.json( { limit: config.serverMaxPostSize } ) );
app.use( express.urlencoded( { limit: config.serverMaxPostSize } ) );

const server = app.listen( config.serverPort, function () {
  console.log( 'Server listening on port ' + config.serverPort );
} );

server.setTimeout( config.serverTimeoutMs );

function downloadAll( links, params, callback ) {
  const pages = [];
  const delay = 10;
  const totalLinks = links.length;

  let numOpenedQueries = 0;
  let currentLinkIndex = 0;
  let errors = [];
  let loaded = 0;
  // если какие то страницы ни как не удаётся загрузить, может
  // возникнуть зацикливание. Эти переменные помогут его избежать
  let numUrlsInLastLoading = -1;
  let numUrlsInPreviousLoading = 0;

  const interval = setInterval( () => {
    // all pages was loaded
    if ( loaded === links.length ) {
      console.log( 'Loaded ' + pages.length + '/' + totalLinks );

      // all url successfully loaded
      if ( pages.length === totalLinks ) {
        callback( pages );
        clearInterval( interval );
        return;
      }

      // try load again no loaded pages
      console.log( 'RELOAD ' + errors.length + ' links' );

      // is empty load
      if ( numUrlsInLastLoading === errors.length ) {
        let numPagesNotLoaded = totalLinks - pages.length;
        console.log( 'CIRCLE FOUND; ' + numPagesNotLoaded + ' pages not loaded' );
        callback( pages.concat( errors ) );
        clearInterval( interval );
        return;
      }

      numUrlsInLastLoading = numUrlsInPreviousLoading;
      numUrlsInPreviousLoading = errors.length;
      currentLinkIndex = 0;
      loaded = 0;
      links = errors.map( ( item ) => item.link );
      errors = [];
    }

    load();
  }, delay );

  function load() {
    if ( ( params[ 'max-opened-queries' ] || config.maxOpenedQueries ) === numOpenedQueries ) {
      return;
    }

    if ( currentLinkIndex === links.length ) {
      return;
    }

    const link = links[ currentLinkIndex ];
    const reqCoreOptions = {
      url: encodeURI( decodeURI( link.url ) ),
      method: link[ 'method' ],
      headers: link[ 'headers' ],
      timeout: params[ 'timeout' ],
      strictSSL: false,
    };

    switch ( link[ 'method' ] ) {
      case 'GET':
        reqCoreOptions[ 'qs' ] = link[ 'form' ];
        break;
      case 'POST':
        reqCoreOptions[ 'form' ] = link[ 'form' ];
    }

    if ( params[ 'use-proxy' ] === true ) {
      reqCoreOptions.proxy = 'https://' + Proxy.getRandomProxy( link.url );
    }

    currentLinkIndex++;
    numOpenedQueries++;

    request( reqCoreOptions, function ( err, res, result ) {
      numOpenedQueries--;
      loaded++;

      if ( err ) {
        errors.push( { link, result, err: { code: err.code, message: err.message, } } );
        return;
      }

      if ( result === '' ) {
        errors.push( { link, result, err: { code: 'EMPTY_RESPONSE', message: 'server did not sent any data', } } );
        return;
      }

      pages.push( { link, result } );
    } );
  }

  load();
}

let loadComment = '';

app.post( '/fetch', jsonParser, function ( req, res ) {
  const params = {
    'max-opened-queries': 50,
    'use-proxy': false,
    'headers': {
      'User-Agent': 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us) AppleWebKit/533.4+ (KHTML, like Gecko) Version/4.0.5 Safari/531.22.7',
    },
    'call-down-s': 0,
    'method': 'GET',
    'timeout': config.requestTimeoutMs,
  };

  // copy all load function parameters
  for ( let key in req.body ) {
    if ( params[ key ] !== undefined && req.body.hasOwnProperty( key ) ) {
      if ( typeof params[ key ] === "object" ) {
        params[ key ] = { ...params[ key ], ...req.body[ key ] };
      } else {
        params[ key ] = req.body[ key ];
      }
    }
  }

  // comment to loading
  if ( req.body[ 'comment' ] ) {
    loadComment = ' — ' + req.body[ 'comment' ];
  }

  console.log( `BEGIN PROGRAM${ loadComment }` );

  if ( params[ 'call-down-s' ] > 0 ) {
    console.log( 'CALL DOWN ' + params[ 'call-down-s' ] + 'sec.' );
  }

  setTimeout( function () {
    console.log( 'Loading ' + req.body.links.length + ' urls by ' + params[ 'max-opened-queries' ] + ' in one time.' );
    downloadAll( req.body.links, params, function ( result ) {
      res.json( result );
      console.log( "END PROGRAM\n\r" );
    } );
  }, 1000 * params[ 'call-down-s' ] );
} );
