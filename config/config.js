module.exports = {
  "proxyListUrl": "https://checkerproxy.net/api/archive",
  "serverMaxPostSize": "50000mb",
  "serverPort": 3000,
  "serverTimeoutMs": 1000 * 60 * 60 * 2,
  "requestTimeoutMs": 1000 * 10,
  "maxOpenedQueries": 20,
  "maxProxyTimeoutS": 5,
}
